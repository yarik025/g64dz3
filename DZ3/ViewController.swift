//
//  ViewController.swift
//  DZ3
//
//  Created by Yaroslav Georgievich on 17.07.2018.
//  Copyright © 2018 Yaroslav Georgievich. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        //character()
        middleName()
        
    }
    //1
    func character() {
        let myName = "Yaroslav Pavliuk"
        print("Символов в строке: \(myName.count)")
    }
    //2
    func middleName() {
        let myMiddleName = "Georgievich"
        let firstSuffix = "ich"
        let secondSuffix = "na"
        
        if myMiddleName.hasSuffix(firstSuffix) {
            print("Отчество заканчиваеться на: \(firstSuffix)")
        }
        else if myMiddleName.hasSuffix(secondSuffix) {
            print("Отчество заканчиваеться на: \(secondSuffix)")
        }
        else {
            print("нету такого суфикса")
        }
    }
}

